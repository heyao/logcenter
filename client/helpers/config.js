Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});


UI.registerHelper("formatDate", function(datetime, format) {
  if (moment) {
            return moment(datetime).format(DateFormats[format]);
  }else {
  	return datetime;
  }
});