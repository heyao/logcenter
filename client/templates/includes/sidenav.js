Template._sidenav.helpers({
  collections: function() {
    return findLogCollections();
  }
});

isActive = function(path) {
	return Router.current() .path == path;
}