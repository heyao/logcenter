Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
  waitOn: function() { 
    Meteor.subscribe('log_postman');
    return Meteor.subscribe('log_openapi');
  }
});

Router.route('/',{name: 'home'});

Router.route('/logs/:dbname',function () {
      var params = this.params;
      dbname = params.dbname;
      console.log(dbname);
      this.render('logsList', {
      data: function () {
        return {
          logs:findLogs(dbname)
        };
      }
    });
   },{
      name: 'logs.show'
});





var requireLogin = function() {
  if (! Meteor.user()) {
     if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
};


