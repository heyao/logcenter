//server and clients share
log_collections = {
	log_postman: new Mongo.Collection('log_postman'),
	log_openapi : new Mongo.Collection('log_openapi')
};

log_collections['log_postman'].allow({

});



//找出以log_开头的collction
findLogCollections = function() {
	ALL_Coll = new Mongo.Collection.getAll();
	 return _.filter(ALL_Coll, function(coll){ 
	            return coll.name && coll.name.match('log_')
	 });
}



//根据collction name 查出这个collction的数据 默认100行
findLogs = function(collection_name,_limit) {
	console.log(collection_name);
	if (_.indexOf(_.keys(log_collections),collection_name) ===  -1) {
		console.error('没有这个集合.');
		return;
	}

	return log_collections[collection_name].find({},{sort:{_id:-1},limit:_limit?_limit:100});
}


DateFormats = {
       simple: "YY/MM/DD",
       normal: "YYYY MM DD HH:mm",
       fulll: "dddd DD.MM.YYYY HH:mm"
};

